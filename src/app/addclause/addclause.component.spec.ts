import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddclauseComponent } from './addclause.component';

describe('AddclauseComponent', () => {
  let component: AddclauseComponent;
  let fixture: ComponentFixture<AddclauseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddclauseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddclauseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
