import { Component, OnInit } from '@angular/core';
import { LogService } from '../log.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  isUserLogged: boolean;

  constructor(private logService: LogService) { }
  
  ngOnInit() {
    this.logService.currentStatus.subscribe(isUserLogged => {
      console.log("isUserlogged", isUserLogged);
      this.isUserLogged = isUserLogged;
    });
  }
  
  changeLogged() {
    this.logService.changeLoggedStatus(true);
  }
}
