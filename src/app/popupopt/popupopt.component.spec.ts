import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupoptComponent } from './popupopt.component';

describe('PopupoptComponent', () => {
  let component: PopupoptComponent;
  let fixture: ComponentFixture<PopupoptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupoptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupoptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
