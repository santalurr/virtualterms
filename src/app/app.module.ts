import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { SignUpComponent } from './signup/signup.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { routing } from './app.routes';
import { EditorComponent } from './editor/editor.component';
import { LandingComponent } from './landing/landing.component';
import { AddclauseComponent } from './addclause/addclause.component';
import { CustomclauseComponent } from './customclause/customclause.component';
import { ChoosetypeComponent } from './choosetype/choosetype.component';
import { SelecttemplateComponent } from './selecttemplate/selecttemplate.component';
import { SelectsavedComponent } from './selectsaved/selectsaved.component';
import { SearchbarComponent } from './searchbar/searchbar.component';
import { PreviewComponent } from './preview/preview.component';
import { AdminlandingComponent } from './adminlanding/adminlanding.component';
import { ManageclausesComponent } from './manageclauses/manageclauses.component';
import { ManagedefinitionsComponent } from './managedefinitions/managedefinitions.component';
import { DefinitionComponent } from './definition/definition.component';
import { ClauseComponent } from './clause/clause.component';
import { TemplateComponent } from './template/template.component';
import { SectionComponent } from './section/section.component';
import { PopupComponent } from './popup/popup.component';
import { PopupoptComponent } from './popupopt/popupopt.component';
import { AdminbannerComponent } from './adminbanner/adminbanner.component';
import { PricingComponent } from './pricing/pricing.component';
import { HomeComponent } from './home/home.component';
import { LogService } from './log.service';


@NgModule({
  declarations: [
    AppComponent,
    SignUpComponent,
    LoginComponent,
    NavbarComponent,
    FooterComponent,
    LoginComponent,
    EditorComponent,
    LandingComponent,
    AddclauseComponent,
    CustomclauseComponent,
    ChoosetypeComponent,
    SelecttemplateComponent,
    SelectsavedComponent,
    SearchbarComponent,
    PreviewComponent,
    AdminlandingComponent,
    ManageclausesComponent,
    ManagedefinitionsComponent,
    DefinitionComponent,
    ClauseComponent,
    TemplateComponent,
    SectionComponent,
    PopupComponent,
    PopupoptComponent,
    AdminbannerComponent,
    PricingComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    routing
  ],
  providers: [LogService],
  bootstrap: [AppComponent]
})
export class AppModule { }
