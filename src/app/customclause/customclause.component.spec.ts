import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomclauseComponent } from './customclause.component';

describe('CustomclauseComponent', () => {
  let component: CustomclauseComponent;
  let fixture: ComponentFixture<CustomclauseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomclauseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomclauseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
