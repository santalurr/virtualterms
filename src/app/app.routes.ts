import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignUpComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { EditorComponent } from './editor/editor.component';
import { AddclauseComponent } from './addclause/addclause.component';
import { CustomclauseComponent } from './customclause/customclause.component';
import { LandingComponent } from './landing/landing.component';
import { ChoosetypeComponent  } from './choosetype/choosetype.component';
import { SelecttemplateComponent  } from './selecttemplate/selecttemplate.component';
import { SelectsavedComponent  } from './selectsaved/selectsaved.component'
import { PreviewComponent } from './preview/preview.component';
import { AdminlandingComponent } from './adminlanding/adminlanding.component';
import { ManageclausesComponent } from './manageclauses/manageclauses.component';
import { ManagedefinitionsComponent } from './managedefinitions/managedefinitions.component';
import { DefinitionComponent } from './definition/definition.component';
import { ClauseComponent } from './clause/clause.component';
import { TemplateComponent } from './template/template.component';
import { SectionComponent } from './section/section.component';
import { PopupComponent } from './popup/popup.component';
import { PopupoptComponent } from './popupopt/popupopt.component';
import { PricingComponent } from './pricing/pricing.component';
import { HomeComponent } from './home/home.component';

// Route Configuration
export const routes: Routes = [
    { path: 'signup', component: SignUpComponent },
    { path: 'home', component: HomeComponent },
    { path: 'login', component: LoginComponent },
    { path: 'editor', component: EditorComponent },
    { path: 'addclause', component: AddclauseComponent },
    { path: 'customclause', component: CustomclauseComponent },
    { path: 'landing', component: LandingComponent },
    { path: 'choosetype', component: ChoosetypeComponent },
    { path: 'selecttemplate', component: SelecttemplateComponent },
    { path: 'selectsaved', component: SelectsavedComponent },
    { path: 'preview', component: PreviewComponent },
    { path: 'adminlanding', component: AdminlandingComponent },
    { path: 'manageclauses', component: ManageclausesComponent },
    { path: 'managedefinitions', component: ManagedefinitionsComponent },
    { path: 'definition', component: DefinitionComponent },
    { path: 'clause', component: ClauseComponent },
    { path: 'template', component: TemplateComponent },
    { path: 'section', component: SectionComponent },
    { path: 'popup', component: PopupComponent },
    { path: 'popupopt', component: PopupoptComponent },
    { path: 'pricing', component: PricingComponent },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
