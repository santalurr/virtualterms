import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectsavedComponent } from './selectsaved.component';

describe('SelectsavedComponent', () => {
  let component: SelectsavedComponent;
  let fixture: ComponentFixture<SelectsavedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectsavedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectsavedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
