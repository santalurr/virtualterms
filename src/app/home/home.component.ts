import { Component, OnInit } from '@angular/core';
import { LogService } from '../log.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  isUserLogged: boolean;

  constructor(private logService: LogService) { 
    this.logService.changeLoggedStatus(false);
  }

  ngOnInit() {
    this.logService.currentStatus.subscribe(isUserLogged => this.isUserLogged = isUserLogged);  
  }

}
