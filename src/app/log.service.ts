import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class LogService {

  private isLoggedSource = new BehaviorSubject<boolean>(true);
  currentStatus = this.isLoggedSource.asObservable();

  constructor() { }

  changeLoggedStatus(status: boolean) {
    this.isLoggedSource.next(status);
  } 
}
