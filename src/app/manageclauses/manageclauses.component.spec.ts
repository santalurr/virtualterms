import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageclausesComponent } from './manageclauses.component';

describe('ManageclausesComponent', () => {
  let component: ManageclausesComponent;
  let fixture: ComponentFixture<ManageclausesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageclausesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageclausesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
