import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagedefinitionsComponent } from './managedefinitions.component';

describe('ManagedefinitionsComponent', () => {
  let component: ManagedefinitionsComponent;
  let fixture: ComponentFixture<ManagedefinitionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagedefinitionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagedefinitionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
